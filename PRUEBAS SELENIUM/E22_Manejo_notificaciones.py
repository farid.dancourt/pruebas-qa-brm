# Rutina para permitir o bloquear las notificaciones en una página web donde sale el pop-up de notificaciones.
# Se toma de ejemplo la página de Bancolombia, la cual presenta ese pop-up (el cual no es un alert):

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

opciones = Options()

# Enviar argumentos (1: Permite la notificación. 2: Bloquea la notificación):
opciones.add_experimental_option("prefs",{
    "profile.default_content_setting_values.notifications" : 2
    })

driver = webdriver.Chrome(chrome_options=opciones, executable_path=r"/usr/local/share/chromedriver")
driver.get('https://www.bancolombia.com/centro-de-ayuda/canales/sucursal-virtual-personas')
time.sleep(3)

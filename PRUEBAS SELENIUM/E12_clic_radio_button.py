# Rutina para hacer clic en un radio button o un checkbox:

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_radio_button(self):
        driver = self.driver
        driver.get('https://www.klappme.co/login-klappme/')
        time.sleep(3)
        # Busca el checkbox "Recordarme" de la página y le da clic:
        radio_button = driver.find_element(By.XPATH, '/html/body/div[2]/div/div/div[2]/div[2]/div/form/div[3]/input')
        radio_button.click()
        time.sleep(5)
        radio_button.click()
        time.sleep(3)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()
# Rutina para recuperar la lista de búsquedas sugeridas en el buscador de Google
# NOTA: el video tutorial de esta rutina es el 23 del curso de Python con Selenium (Capítulo Extra # 2)

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

palabra_busqueda = 'seleni'
driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('https://www.google.com')
time.sleep(3)

busqueda = driver.find_element(By.NAME, 'q') # Busca la barra de búsqueda de Google
busqueda.send_keys(palabra_busqueda) # coloca la palabra 'seleni' en la barra de búsqueda
time.sleep(3)

for i in range(1,11): # Rango de 1 a 11 ya que el buscador de Google presenta por default 11 palabras sugeridas
    elementos = driver.find_element(By.XPATH, '/html/body/div[1]/div[3]/form/div[1]/div[1]/div[2]/div[2]/div[2]/ul[1]/div/ul/li['+str(i)+']/div/div[2]/div[1]/span/b').text
    # OJO: en la línea de arriba cambié la lista li[1] por li['+str(i)+'] ya que 'i' es la variable temporal del ciclo for
    # OJO: al final de la línea de arriba se colocó .text para que el sistema recupere el texto y lo asigne a la variable 'elementos'
    print(palabra_busqueda + elementos) # Se concatenan para que la palabra se muestre completa (seleni + x, donde x es la palabra obtenida de la búsqueda)
driver.close()



# Abre la página de google en una pestaña del navegador; luego crea otra pestaña y en ella abre
# la página de klappme; luego vuelve a la primera pestaña y se cierra el programa

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_cambiar_ventana(self):
        driver = self.driver
        driver.get("https://www.google.com/") # abre la página de google, lo que sería la pestaña 0
        time.sleep(3)
        driver.execute_script("window.open('');") # forma de abrir una nueva ventana web en Python. Se abre
                                                  # entonces una nueva pestaña, que sería la pestaña 1
        time.sleep(3)
        driver.switch_to.window(driver.window_handles[1]) # Se le está diciendo que se posicione en la
                                                           # pestaña 1
        driver.get("https://www.klappme.co/") # En la pestaña 1, abra la página de klappme
        time.sleep(3)
        driver.switch_to.window(driver.window_handles[0]) # se posiciona en la pestaña 0 (la de google)
        time.sleep(3)

if __name__ == "__main__":
    unittest.main()



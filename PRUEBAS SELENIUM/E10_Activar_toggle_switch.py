# Rutina que activa y desactiva un toggle o switch en una página (www.w3schools.com/howto/howto_css_switch.asp)

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_toggle(self):
        driver = self.driver
        driver.get('https://www.w3schools.com/howto/howto_css_switch.asp')
        time.sleep(3)
        toggle = driver.find_element(By.XPATH, '/html/body/div[7]/div[1]/div[1]/label[3]/div') # Nota: tuve que copiar
                                                                                # el full Xpath ya que con el Xpath
                                                                                # solo no funciona
        toggle.click() # hace un clic al toggle
        time.sleep(3)
        toggle.click() # hace otro clic para volvewr el toggle a su estado original
        time.sleep(3)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()





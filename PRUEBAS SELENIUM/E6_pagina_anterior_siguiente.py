import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_pagina_siguiente_anterior(self):
        driver = self.driver
        driver.get("https://www.google.com/") # abre la página de google
        time.sleep(3)
        driver.get("https://www.klappme.co/")  # abre la página de klappme en la misma pestaña
        time.sleep(3)
        driver.get("https://tarkuslab.com/telemedicina/")  # abre ahora la página de telemedicina de tarkus
        time.sleep(3)
        driver.back() # regresa a la página de klappme
        time.sleep(3)
        driver.back() # regresa a la página de google
        time.sleep(3)
        driver.forward() # vuelve a la página de klappme
        time.sleep(3)

if __name__ == "__main__":
    unittest.main()
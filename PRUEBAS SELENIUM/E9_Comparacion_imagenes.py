# Compara dos imágenes resaltando sus diferencias
# Nota: No funciona la primera parte donde se crea el archivo Img2 (solo lo crea con el Debug)
import time
import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import cv2

class usando_unittest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_opencv(self):
        driver = self.driver
        driver.get("https://www.google.com/")
        time.sleep(5)
        driver.save_screenshot('Img2.png') # hace una captura de pantalla y la guarda en archivo con nombre Img2.png
        time.sleep(5)

    def test_comparar_imagenes(self):
        imagen1 = cv2.imread('Img1.png') # Carga la imagen Img1.png en la variable imagen1
        imagen2 = cv2.imread('Img2.png') # Carga la imagen Img2.png en la variable imagen2
        diferencia = cv2.absdiff(imagen1, imagen2) # Hace la comparación de las imágenes en la variable 'diferencia'
        imagen_gris = cv2.cvtColor(diferencia, cv2.COLOR_BGR2GRAY) # cambia el color de las imágenes a escala de grises
                                                                   # para que se facilite al sisteam la comparación
        contours,_ = cv2.findContours(imagen_gris, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE) # manejo de contornos
                                                                                               # (averiguar bien)
        for c in contours: # se hace un ciclo for para que se haga una comparación cíclica de las imágenes (la variable
                           # 'c' es un contador)
            if cv2.contourArea(c) >= 20: # el 20 indica la escalación a nivel de grises. Si se escoge un valor inferior,
                                         # se hace más fina la comparación, pero eso puede generar más errores
                posicion_x, posicion_y, ancho, alto = cv2.boundingRect(c) # Si se cumple la condición, se guardan las
                                                                          # coordenadas de la zona en la que difieren
                                                                          # las dos imágenes
                # A continuación, se dibuja entonces un rectángulo en la zona donde se encuentra la diferencia entre
                # las imágenes. Los valores dentro del paréntesis (0,0,255) indican color negro:
                cv2.rectangle(imagen1,(posicion_x,posicion_y),(posicion_x+ancho,posicion_y+alto),(0,0,255),2)

        while(1):
            cv2.imshow('Imagen 1',imagen1) # mostar la imagen 1
            cv2.imshow('Imagen 2', imagen2) # mostar la imagen 2
            cv2.imshow('Diferencias encontradas', diferencia) # mostar las diferencias
            # Rutina para que se cierren las ventanas con la tecla ESC:
            teclado = cv2.waitKey(5) & 0xFF
            if teclado == 27: # La tecla ESC es la número 27
                break
        cv2.destroyAllWindows() # cierra todas las ventanas

if __name__ == "__main__":
    unittest.main()








# Rutina para ubicar un link en la página y hacer clic sobre él.

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('https://tarkuslab.com/telemedicina/')
time.sleep(3)
encontrar_link = driver.find_element(By.LINK_TEXT, 'Equipo médico') # Busca el link con ese nombre
encontrar_link.click() # hace clic sobre el link encontrado
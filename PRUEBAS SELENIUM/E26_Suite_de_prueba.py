import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

class suite(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_busqueda(self): # Coloca la palabra "selenium" en el buscador de Google y ejecuta la búsqueda
        self.driver.get("https://www.google.com/")
        self.busqueda = self.driver.find_element(By.NAME,"q")
        self.busqueda.send_keys("selenium")
        self.busqueda.submit()
        time.sleep(3)

    def test_scroll_dwon(self): # Busca la palabra "celular" en Linio y se va hasta el final de la página
        self.driver.get("https://www.linio.com.co/search?scroll=&q=celular") # Busco en Linio la palabra "celular"
        time.sleep(3)
        self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)") # Orden para ir hasta el final de la página)
        time.sleep(3)

    def test_link_por_texto(self): # Busca el link "Equipo médico" en la página de Tarkus y hace clic en él
        self.driver.get('https://tarkuslab.com/telemedicina/')
        time.sleep(3)
        encontrar_link = self.driver.find_element(By.LINK_TEXT, 'Equipo médico')  # Busca el link con ese nombre
        encontrar_link.click()  # hace clic sobre el link encontrado
        time.sleep(3)

    def tearDown(self): # finalización para limpieza de variables
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
# Esta rutina puede ser útil para probar que el nombre de los links de la página sean los correctos

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains # Librería para manejo de posición del mouse
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get("https://tarkuslab.com/telemedicina/")
time.sleep(3)
elem = driver.find_element(By.LINK_TEXT,"Blog") # Busca el elemento de link con nombre 'Blog' en la página
                                                      # Ojo: nótese que aquí no se necesita inspeccionar el elmento
hover = ActionChains(driver).move_to_element(elem) # Se ubica sobre el elemento buscado (elem)
hover.perform() # Hace que el subrayado del link se vea, como si el puntero del mouse estuviera sobre el link

# Rutina para buscar un elemento por css y hacer clic en él

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('https://www.w3schools.com/html/default.asp')
time.sleep(3)
content = driver.find_element(By.CSS_SELECTOR, 'a.w3-blue') # Busca el elemento 'Start HTML Quiz' por css
content.click()

# Rutina para verificar si un elemento de la página se carga correctamente y si está disponible para usarlo

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get("https://tarkuslab.com/telemedicina/")
# Busca el elemento "Solicita tu cita médica" por Xpath:
displayelement = driver.find_element(By.XPATH, '/html/body/div[4]/div/div[2]/div/a')
print(displayelement.is_displayed()) # regresa True o False si la página ya cargó el elemento
print(displayelement.is_enabled()) # regresa True o False si el elemento está disponible para hacer clic

# Resultado: en ambos casos el resultado fue True ya que el elemento se cargó bien y está disponible

# Se abre la página de google.com y se obliga al sistema a esperar máximo 10 segundos a que aparezca la
# barra de búsqueda. De esta forma, si por alguna falla no se carga la barra de búsqueda, el programa
# indica el error adecuadamente. Eso evita también estar usando sleeps manuales a cada rato, que hacen más
# demorada la automatización.

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC # módulo para llamar el explicit o el implicit
                                                                 # wait

class usando_unittest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_explicit_wait(self):
        driver = self.driver
        driver.get("https://www.google.com/")
        try:
            element = WebDriverWait(driver,10).until(EC.presence_of_element_located((By.NAME, "q")))
                    # Instrucción para que el sistema espere 10 segundos a que aparezca la barra de búsqueda
                    # de google (cuyo Id es 'q')
        finally:
            driver.quit() # siempre es recomendable cerrar el driver

if __name__ == "__main__":
    unittest.main()


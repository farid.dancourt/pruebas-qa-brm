'''Rutina para hacer clic en el botón de una ventana emergente tipo confirm (confirm alert)
Primero se hizo una pequeña página local en html cuyo archivo se llama Confirm_Alert.html
Esta pequeña página tiene un botón que llama a una ventana emergente tipo confirm'''
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('file:///home/brm/Escritorio/PHYTON/PRUEBAS%20SELENIUM/Confirm_Alert.html') # abre la página html
# OJO: En la línea anterior, para obtener la ruta del archivo html, fue necesario abrir el archivo desde el
# explorador de archivos
alerta_confirm = driver.find_element(By.NAME, "alerta_tipo_confirm") # busca el botón que genera la ventana emergente
time.sleep(2)
alerta_confirm.click() # hace clic en el botón para generar la ventana emergente
time.sleep(3)
alerta_confirm = driver.switch_to.alert # se cambia a la ventana emergente
alerta_confirm.dismiss() # El método dismiss hace clic en el botón 'Cancelar' de la confirm alert
alerta_confirm.accept() # El método accept hace clic en el botón 'Aceptar' de la confirm alert
driver.close()
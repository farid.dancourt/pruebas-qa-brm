# Rutina para obtener los datos de una tabla que está en una página web:
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('https://www.w3schools.com/html/html_tables.asp')
time.sleep(3)
# A continuación busco la tabla por Xpath y guardo sus datos en una variable llamada 'valor':
valor = driver.find_element(By.XPATH, '/html/body/div[7]/div[1]/div[1]/div[3]/div/table/tbody/tr[1]').text
# Pero ojo: solamente me va a guardar los datos de la primera fila de la tabla (los títulos):
print(valor) # Rta: muestra solamente los títulos de la tabla (la primera fila)

'''Para que muestre todos los datos de la tabla, debo modificar el 'tr[1]' del xpath dentro de un ciclo for, pero
primero necesito saber cuántas filas tiene la tabla:'''
filas = len(driver.find_elements(By.XPATH, '/html/body/div[7]/div[1]/div[1]/div[3]/div/table/tbody/tr'))
print(filas) # Rta: 7. La tabla tiene 7 filas. Esta línea es solo para ver en pantalla la cantidad de filas de la tabla

for n in range(1,filas+1): # Se coloca filas+1 ya que el range no incluye el límite superior
    valor = driver.find_element(By.XPATH, '/html/body/div[7]/div[1]/div[1]/div[3]/div/table/tbody/tr['+str(n)+']').text
    print(valor)
driver.close()

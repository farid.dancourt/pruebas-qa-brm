'''Rutina para el uso de sroll down. Útil en páginas que van cargando información a medida que me voy desplazando
en ellas hacia abajo (p.e. Linio, Amazon, Aliexpress, y por lo general, páginas de ventas). Una buena utilidad es
para poder ir al final de dichas páginas y verificar el Footer. También útil para que se carguen todos lo elementos
de la página de una vez y no tener que ir desplazándose para que se vayan cargando '''

from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get("https://www.linio.com.co/search?scroll=&q=lampara") # Busco en Linio la palabra "memoria"
time.sleep(3)
driver.execute_script("window.scrollTo(0,document.body.scrollHeight)") # Orden para ir hasta el final de la página
time.sleep(3)


'''Rutina para hacer clic en el botón de una ventana emergente simple (alerta simple)
Primero se hizo una pequeña página local en html cuyo archivo se llama Alerta_simple.html
Esta pequeña página tiene un botón que llama a una ventana emergente simple'''
from selenium import webdriver
from selenium.webdriver.common.by import By
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('file:///home/brm/Escritorio/PHYTON/PRUEBAS%20SELENIUM/Alerta_simple.html') # abre la página html
# OJO: En la línea anterior, para obtener la ruta del archivo html, fue necesario abrir el archivo desde el
# explorador de archivos
alerta_simple = driver.find_element(By.NAME, "alerta") # busca el botón que genera la ventana emergente
time.sleep(2)
alerta_simple.click() # hace clic en el botón para generar la ventana emergente
time.sleep(3)
alerta_simple = driver.switch_to.alert # se cambia a la ventana emergente
#alerta_simple.dismiss() # El método dismiss hace clic en el botón único de la alerta simple (ventana emergente)
alerta_simple.accept() # El método accept hace clic en el botón único de la alerta simple (ventana emergente)
driver.close()

# El Implicit Wait funciona igual que el time.sleep. La diferencia es que el primero es de Selenium y el
# segundo es exclusivo de Python. Por lo demás, no tienen ninguna diferencia. Así que, si se usa Python, es
# más práctico usar el time.sleep; pero si se usa otro lenguaje con Selenium, se debe usar Implicit Wait para
# hacer pausas de tiempo.

import unittest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class usando_unittest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_implicit_wait(self):
        driver = self.driver
        driver.implicitly_wait(10) # Esperar 5 segundos antes de abrir la página
        driver.get("https://www.google.com/")
        myDinamicElement = driver.find_element(By.NAME,'q') # Esto no hace parte del ejercicio de implicit
                                                            # wait, es una función útil para ubicar elementos
                                                            # que son dinámicos, es decir, que pueden cambiar
                                                            # su nombre en cualquier momento (revisar).

if __name__ == "__main__":
    unittest.main()

# NOTA: No me parece que esté funcionando el implicit wait acá.
# xpath es una esructura de objetos (p.e. árbol de carpetas)
# Hay dos tipos de xpath: Relativo y Absoluto
# xpath Relativo: //*[@id="input"] (xpath relativo de la barra de búsqueda de google.com)
# xpath Absoluto: /html/body/ntp-app//div/ntp-realbox//div/input (xpath absoluto de la misma barra)

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_buscar_por_xpath(self):
        driver = self.driver
        driver.get("https://www.google.com/")
        time.sleep(3)
        elemento = driver.find_element(By.XPATH, "html/body/div[1]/div[3]/form/div[1]/div[1]/div[1]/div/div[2]/input") # barra de búsqueda de google
        time.sleep(3)
        elemento.send_keys("selenium", Keys.ARROW_DOWN) # coloca la palabra selenium y aplica la flecha
                                                        # hacia abajo de autocompletado
        time.sleep(3)

    def tearDown(self):
        self.driver.close()

if __name__ == "__main__":
    unittest.main()


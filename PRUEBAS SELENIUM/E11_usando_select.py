# Rutina que recorre un menú de opciones de marcas de autos de la página
# www.w3schools.com/howto/howto_custom_select.asp. Imprime en pantalla los valores internos de la lista de opciones
# para luego escoger el auto de la posición 10 (Nissan)

import unittest
from selenium import webdriver
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_usando_select(self):
        driver = self.driver
        driver.get('https://www.w3schools.com/howto/howto_custom_select.asp')
        time.sleep(3)
        select = driver.find_element(By.XPATH, '/html/body/div[7]/div[1]/div[1]/div[3]/div[1]/select')
        opcion = select.find_elements_by_tag_name('option') # Guarda en la variable opcion todas las opciones del
                                                            # select. Ojo: se coloca elements y no element porque se van
                                                            # a cargar todas las opciones en la variable. Nota: el
                                                            # nombre del tag name (option) es tomado al inspeccionar el
                                                            # elemento en la página
        time.sleep(3)
        for option in opcion:
            print(f'Los valores son: {option.get_attribute("value")}') # Muestra en pantalla todas las opciones del
                                                                        # elemento select
            option.click()
            time.sleep(3)
        seleccionar = Select(driver.find_element(By.XPATH, '/html/body/div[7]/div[1]/div[1]/div[3]/div[1]/select'))
        seleccionar.select_by_value('10') # Selecciona el elemento 10 del select (Nissan)
        time.sleep(3)

if __name__ == "__main__":
    unittest.main()




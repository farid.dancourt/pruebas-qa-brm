'''Rutina que devuelve en pantalla las cookies de una página web'''

from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")
driver.get('https://tarkuslab.com/telemedicina/')
time.sleep(3)
all_cookies = driver.get_cookies()
print(all_cookies)
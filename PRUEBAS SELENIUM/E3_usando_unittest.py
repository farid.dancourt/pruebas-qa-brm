# Ejemplo de uso de funciones para hacer un test al buscador de Google haciendo que el Python
# me de un resultado positivo o negativo de acuerdo al resultado del test

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import time

class usando_unittest(unittest.TestCase):

    def setUp(self): # se declara una función setup para inicializar nuestro driver
        self.driver = webdriver.Chrome(executable_path=r"/usr/local/share/chromedriver")

    def test_buscar(self): # Ojo: el nombre de todas las funciones después del setup deben empezar por
                           # test ya que el unittest va a buscar las funciones con ese nombre
        driver = self.driver # cargamos nuestro driver
        driver.get("https://www.google.com/") # carga la página de Google
        self.assertIn("Google", driver.title) # verifica que el título de la pestaña sí sea Google para
                                              # confirmar que la página es la correcta
        elemento = driver.find_element(By.NAME, "q") # busca la barra de búsqueda
        elemento.send_keys("selenium") # coloca la palabra selenium en la barra de búsqueda de Google
        elemento.send_keys(Keys.ENTER) # hace Enter (se puede usar también RETURN, es igual)
        time.sleep(5) # pausa por 5 segundos
        assert "No se encontró el elemento:" not in driver.page_source # resultado si no encontró el
                                                                       # título esperado

    def tearDown(self): # función para cerrar
        self.driver.close()
if __name__ == "__main__":
    unittest.main()             # Esto es para que corra la clase. Sin eso, la clase no corre

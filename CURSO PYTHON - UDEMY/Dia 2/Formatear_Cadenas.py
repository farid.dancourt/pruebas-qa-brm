x = 5
y = 10
# Mostrar las variables dentro de una frase sin formatear cadenas:
print("Mis números son " + str(x) + " y " + str(y))

# Ahora usando la función Format, para versiones de Python anteriores a la 3.8:
print("Mis números son {} y {}".format(x,y))

# Se pueden incluir operaciones dentro de la función Format:
print("La suma de {} y {} es igual a {}".format(x,y,x+y))

# Ahora un ejemplo con la función Format pero antes de la frase - Versiones 3.8 en adelante:
marca_auto = "Renault Logan"
modelo_auto = 2011
color_auto = "Rojo"
print(f"Mi auto es un {marca_auto} modelo {modelo_auto} de color {color_auto}")
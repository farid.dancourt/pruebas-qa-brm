valor = round(95.666666666,2)
print(valor) # El resultado muestra dos decimales
valor = round(95.666666666)
print(valor) # El resultado se aproxima al entero más cercano

valor = round(95.666666666)
print(valor)
print(type(valor)) # Es entero porque valor cambió a 96

valor = 95.66666666
print(round(valor))
print(type(valor)) # Es flotante porque valor sigue siendo 95.6666666

# Ojo: no es lo mismo colocar 0 decimales a no colocar nada:
# Sin colocar nada: (Resultado: integer)
num1 = round(13/2)
print(num1)
print(type(num1))
# Colocando 0 decimales: (Resultado: float)
num1 = round(13/2,0)
print(num1)
print(type(num1))
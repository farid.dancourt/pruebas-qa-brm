x = 7
y = 3
z = 2
print(f"La suma de {x} más {y} es igual a {x+y}")
print(f"La resta de {x} menos {y} es igual a {x-y}")
print(f"El producto de {x} con {y} es igual a {x*y}")
print(f"La división entre {x} y {y} es igual a {x/y}")

# División al piso (calcular el cociente):
print(f"{x} dividido al piso con {z} es igual a {x//z}")

# Módulo de la división (residuo):
print(f"El residuo de {x} dividido {z} es {x%z}")

# Exponente:
print(f"{x} elevado a la {y} es igual a {x**y}")

# Raíz cuadrada (elevar a la 1/2):

print(f"La raíz cuadrada de {x} es {x**0.5}")
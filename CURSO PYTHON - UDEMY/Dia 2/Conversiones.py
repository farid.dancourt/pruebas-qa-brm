print("Conversión implícita")
num1 = 20
num2 = 15.6
print(type(num1))
print(type(num2))
num1 = num1 + num2
print(num1)
print(type(num1))
print(type(num2))

print("\nConversión explícita")
num1 = 3.6
print(num1)
print(type(num1))
num2 = int(num1)
print(num2)
print(type(num2))

print("\nEjemplo con input")
edad = input("Ingrese su edad: ")
print(type(edad))
edad = int(edad)
print(type(edad))
edad_nueva = 10 + edad
print(edad_nueva)

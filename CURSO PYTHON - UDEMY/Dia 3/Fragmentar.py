texto = "ABCDEFGHIJKLM"
fragmento = texto[2:5] # Rta: CDE. Muestra desde el índice 2 hasta el 4 (no incuye el 5)
print(fragmento)

fragmento = texto[2:] # Rta: CDEFGHIJKLM. Muestra desde el índice 2 hasta el final
print(fragmento)

fragmento = texto[:5] # Rta: ABCDE. Muestra desde el comienzo hasta el índice 4 (no incluye el 5)
print(fragmento)

fragmento = texto[2:10:2] # Rta: CEGI. Muestra desde el índice 2 hasta el 9 pero saltando de a dos
print(fragmento)

fragmento = texto[::3] # Rta: ADGJM. Muestra desde el índice 0 hasta el último pero saltando de a tres
print(fragmento)

fragmento = texto[::-1] # Rta: MLKJIHGFEDCBA. Muestra toda la cadena pero al revés (salta de uno en uno)
print(fragmento)

fragmento = texto[::-2] # Rta: MKIGECA. Muestra toda la cadena pero al revés (salta de dos en dos)
print(fragmento)

texto = "CPKOCCK"
print("El feligrés le dice al padre " + texto + "\nEl padre responde " + texto[::-1])
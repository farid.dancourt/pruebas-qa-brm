texto = "Este es el texto de Farid"
resultado = texto
print(resultado)

resultado = texto.upper() # Cambia todo a mayúsculas
print(resultado)

resultado = texto[2].upper() # Solo cambia a mayúscula el índice 2 (letra t)
print(resultado)

resultado = texto.lower() # Cambia todo a minúsculas
print(resultado)

resultado = texto.split() # Guarda todas las palabras separadas por espacios en una lista
print(resultado)

resultado = texto.split("t") # El mismo anterior pero usa la letra "t" como separador
print(resultado)

a = "Aprender"
b = "Python"
c = "es"
d = "genial"
e = " ".join([a,b,c,d]) # Une todas las variables separándolas con el espacio
print(e)

e = "-".join([a,b,c,d]) # Une todas las variables separándolas con el signo "-"
print(e)

resultado = texto.find("g") # Hace lo mismo que index, pero si no encuentra el caracter, en lugar de dar error, muestra un -1
print(resultado)

resultado = texto.replace("Farid","todos") # Reemplaza la palabra "Farid" por "todos"
print(resultado)

resultado = texto.replace("e","x") # Reemplaza todas las "e" por "x"
print(resultado)
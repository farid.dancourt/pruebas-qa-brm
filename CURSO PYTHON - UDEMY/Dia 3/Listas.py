mi_lista = ["a", "b", "c"]
mi_lista2 = ["d", "e", "f"]
print(type(mi_lista)) # Muestra la clase (list)

otra_lista = ["hola", 5, 4.87] # Las listas pueden contener diversos tipos de datos

cantidad = len(mi_lista)
print(cantidad) # Muestra la cantidad de elementos en la lista (3)

resultado = mi_lista[0]
print(resultado) # Puedo mostrar un elemento de la lista

resultado = mi_lista[0:2]
print(resultado) # Muestra los elementos con índices 0 y 1 (no incluye el elemento con índice 2)

resultado = mi_lista[0:]
print(resultado) # Muestra todos los elementos de la lista

print(mi_lista + mi_lista2) # Las listas se pueden concatenar

mi_lista3 = mi_lista + mi_lista2
mi_lista3[0] = "alfa"
print(mi_lista3) # Puedo cambiar un índice en una lista (lo que no se puede con los string)

mi_lista3.append("g")
print(mi_lista3) # Puedo agregar elementos a la lista

mi_lista3.pop() # Elimina el último elemento de la lista
print(mi_lista3) # Eliminé el último elemento de la lista

mi_lista3.pop(0) # Elimina el elemento 0 de la lista ("a")
print(mi_lista3)

eliminado = mi_lista3.pop(0) # Elimina el primer elemento de la lista ("b") y lo guarda en eliminado
print(mi_lista3)
print(eliminado)

# Ordenar una lista:
lista_4 = ['f', 'a', 'n', 'd', 'p']
lista_4.sort() # Ordena la lista
print(lista_4)

# Ojo: el método sort no devuelve un resultado, solamente ordena la lista
# por tanto no se puede asignar una variable a un método sort:
lista_5 = lista_4.sort()
print (lista_5) # Devuelve un None ya que lista_5 no tiene tipo asignado
print(type(lista_5)) # Rta: NoneType. Indica que lista_5 no tiene tipo

lista_4.reverse() # Ordena la lista al contrario
print(lista_4)


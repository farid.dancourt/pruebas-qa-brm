mi_texto = "Esta es una prueba"

resultado = mi_texto[0] # Rta: E porque esa letra es la que está en el índice 0
print(resultado)

resultado = mi_texto[6] # Rta: s
print(resultado)

resultado = mi_texto[-1] # Rta: a porque es la última letra
print(resultado)

resultado = mi_texto[-3] # Rta: e
print(resultado)

resultado = mi_texto.index("n") # Rta: 9
print(resultado)

resultado = mi_texto.index("prueba") # Rta: 12 porque en el índice 12 empieza la palabra "prueba"
print(resultado)

resultado = mi_texto.index("a") # Rta: 3 porque index busca de izquierda a derecha
print(resultado)

resultado = mi_texto.rindex("a") # Rta: 17 porque rindex buscar de derecha a izquierda
print(resultado)

resultado = mi_texto.index("a",5,17) # Rta: 10 porque empieza a buscar desde el índice 5 al 17
print(resultado)

resultado = mi_texto.index("a",5,) # Rta: 10. Igual a la anterior, no se coloca el límite superior si no es necesario
print(resultado)

resultado = mi_texto.index("e",5,17) # Rta: 5 porque la búsqueda sí incluye el límite inferior
print(resultado)

resultado = mi_texto.index("e",2,5) # No hay Rta porque la búsqueda no incluye el límite superior
print(resultado)




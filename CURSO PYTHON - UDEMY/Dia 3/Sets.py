mi_set = set([1,2,3,4]) # Los corchetes internos pueden ser curvos o paréntesis
print(type(mi_set))
print(mi_set)

otro_set = {1,2,3,4} # se puede crear también sin usar la palabra set, pero con corchetes curvos
print(type(otro_set))
print(otro_set)

# Nota: Los sets son insuscribibles, es decir, no puedo hacer print para que muestre un solo elemento
# del set. Tampoco puedo modificar los elementos del set.

# Si hay elementos repetidos en el set, Python los elimina automáticamente:
mi_set = {1,2,3,4,2,1,1,1,3}
print(mi_set) # Rta: {1,2,3,4}. Se eliminan los repetidos.

# Los sets son una colección mutable de elementos inmutables, por tanto no pueden contener listas ni
# diccionarios, pero sí tuples:
mi_set = {1,2,3,(1,2),4}
print(mi_set) # Los sets admiten tuples ya que éstos son inmutables.

print(len(mi_set)) # Rta: 5. Cantidad de elementos del set

# Puedo consultar si un elemento está o no dentro de un set:
print(2 in mi_set) # Rta: True. Indica que el elemento 2 sí está en el set (esto se puede hacer too con
                   # las listas, los tuples y los diccionarios; en los diccionarios, se busca por clave)

# Puedo unir sets:
s1 = {1,2,3}
s2 = {3,4,5}
s3 = s1.union(s2) # método para unir sets
print(s3) # Rta: {1,2,3,4,5}. Los sets se unen, eliminando elementos repetidos

s1 = {1,2,3}
s1.add(4) # método para agregar un elemento al set
print(s1) # Rta: {1,2,3,4}

s1 = {1,2,3}
s1.remove(3) # método para eliminar un elemento del set
print(s1) # Rta: {1,2}

s1 = {1,2,3}
s1.discard(3) # método para descartar un elemento del set. Funciona igual que el remove, pero no genera
              # error cuando descarto un elemento que no está en el set
print(s1) # Rta: {1,2}
s1.discard(6) # elimina el elemento 6 del set (ojo: ese elemento no está en el set)
print(s1) # Rta: {1,2}. No pasa nada porque 6 no es elemento del set, pero no genera error en Python,
          # como sí lo haría al utilizar remove.

s1 = {16,2,73,34}
sorteo = s1.pop() # La función pop() elimina un elemento del set al azar
print(s1) # Rta: {73,2,34}. El set queda sin un elemento
print(sorteo) # Rta: 16. Se muestra el elemento eliminado del set

s1 = {2,8,3,45}
s1.clear() # Esta función limpia el set, es decir, elimina todos sus elementos
print(s1) # Rta: set(). Se muestra el set sin elementos






var1 = True
var2 = False
print(type(var1)) # Muestra el tipo de variable (booleana)
print(var1) # Rta: True

numero = 5 > 2+3
print(type(numero))
print(numero) # Rta: False. 5 no es mayor que 5 (2+3)

# Otra forma de escribir lo anterior:
numero = bool(5>2+3)
print(numero)


numero = 5 == 2+3
print(type(numero))
print(numero) # Rta: True. 5 sí es igual a 5 (2+3)

numero = 5 != 2+3
print(type(numero))
print(numero) # Rta: False. 5 no es diferente a 5 (2+3)

numero = bool()
print(numero) # Rta: False. Si no coloco nada entre los paréntesis, Python le asigna siempre False

lista = [1,2,3,4]
control = 5 in lista
print(type(control))
print(control) # Rta: False. El elemento 5 no está dentro de la lista








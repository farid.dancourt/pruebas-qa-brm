diccionario = {'c1':'valor1', 'c2':'valor2'} # Nota: las claves (valores cx) no pueden repetirse, pero
                                             # los valoresx sí
print(type(diccionario)) # Clase dict
print(diccionario)

resultado = diccionario['c1']
print(resultado) # Muestra el valor de la clave c1

# Ejemplo de uso:
paciente = {'nombre':'Farid', 'apellido':'Dancourt', 'peso':76, 'talla':1.75}
consulta = paciente['peso']
print(consulta)

# Un diccionario puede contener diferentes elementos (strings, listas, otros diccionarios, etc):
dic = {'c1':55, 'c2':[10,20,30], 'c3': {'s1':100, 's2':200}} # Diccionario con un string, una lista y un diccionario
print(dic['c2']) # muestra la lista (clave c2)
print(dic['c2'][1]) # muestra el segundo elemento (índice 1) de la lista de la clave 'c2'
print(dic['c3']['s2']) # muestra el valor de la clave 's2' (diccionario dentro de otro diccionario)

# Ejercicio:
dic = {'c1':['a','b','c'], 'c2':['d','e','f']}
# Me piden que, usando una sola línea, se obtenga la letra e, pero en mayúscula
# solución:
print(dic['c2'][1].upper())

# Agregar elemento a un diccinario ya creado:
dic = {1:'a', 2:'b'} # diccionario original
print(dic)
dic[3] = 'c' # así se agrega otra clave al diccionario (clave: 3, valor: 'c')
print(dic)

dic[2] = 'B' # cambia el valor de la clave 2, de 'b' a 'B'
print(dic)

print(dic.keys()) # muestra solamente las claves
print(dic.values()) # muestra solo los valores de las claves
print(dic.items()) # muestra todos los elementos del diccionario



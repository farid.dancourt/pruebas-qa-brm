# El programa debe solicitar lo siguiente al usuario:
# A. Pedir al usuario que ingrese un texto
# B. Pedir al usuario que ingrese 3 letras

# El programa debe hacer lo siguiente:
# 1. Mostrar cuántas veces aparece en el texto cada una de las letras
# 2. Mostrar cuántas palabra tiene el texto
# 3. Mostrar cuál es la primera letra del texto y cuál es la última
# 4. Mostrar el texto invertido
# 5. Decir si la palabra 'Python' aparece en el texto

# A. Pedir a usuario que ingrese un texto:
texto_original = input('Ingrese un texto (frase, poema, enunciado, etc: ')

# B. Pedir a usuario que ingrese tres letras:
letra1 = input('ingrese una letra cualquiera: ')
letra2 = input('ingrese otra letra diferente: ')
letra3 = input('ingrese una última letra distinta a las anteriores: ')

# Pasar todo a minúsculas:
texto = texto_original.lower()
letra1 = letra1.lower()
letra2 = letra2.lower()
letra3 = letra3.lower()

# 1. Cantidad de apariciones de cada letra:
cantidad_letra1 = texto.count(letra1)
cantidad_letra2 = texto.count(letra2)
cantidad_letra3 = texto.count(letra3)
print(f'La letra "{letra1}" aparece {cantidad_letra1} veces en el texto')
print(f'La letra "{letra2}" aparece {cantidad_letra2} veces en el texto')
print(f'La letra "{letra3}" aparece {cantidad_letra3} veces en el texto')

# 2. Cantidad de palabras del texto:
texto_lista = texto.split() # Separo las palabras de acuerdo a los espacios y las ingreso en una lista
print(f'El texto tiene {len(texto_lista)} palabras')

# 3. Primera y última letra del texto:
print(f'La primera letra del texto es "{texto[0]}"')
print(f'La última letra del texto es "{texto[-1]}"')

# 4. Invertir el texto:
texto_original_lista = texto_original.split()
texto_original_lista.reverse() # ordena la lista al revés
print('Mira tu texto al contrario:')
print(' '.join(texto_original_lista)) # une los elementos del texto, separándolos con espacios

# 5. Decir si la palabra Python está en el texto:
python_en_texo = 'python' in texto_lista # Almacena el valor booleano si la palabra python está o
                                            # no en el texto (true o false)
si_o_no = {True:'SÍ', False:'NO'} # Creo un diccionario relacionando True con SÍ y False con NO
print(f'La palabra "Python " {si_o_no[python_en_texo]} está en el texto')
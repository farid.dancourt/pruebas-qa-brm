mi_tuple = (1,2,3,4) # Nota: los tuples pueden ir dentro de paréntesis o sin ellos
print(type(mi_tuple))

t = (5, 7.32, "Hola", ["a",3,5.7]) # los tuples pueden contener diferentes tipos de elementos
print(type(t))

print(mi_tuple[0]) # muestra el elemento de la posición 1 del tuple
print(mi_tuple[-2]) # muestra el segundo elemento de derecha a izquierda del tuple

# NOTA: los tuples son inmutables. No se pueden cambiar sus elementos una vez definido el tuple

mi_tuple = (1,2,(10,20),4) # los tuples se pueden anidar (tuples dentro de tuples)
print(mi_tuple[2]) # Rta: (10, 20)
print(mi_tuple[2][1]) # Rta: 20

mi_tuple = list(mi_tuple) # convierto el tuple en lista
print(type(mi_tuple))
mi_tuple = tuple(mi_tuple)
print(type(mi_tuple)) # convierto la lista en tuple nuevamente

# Puedo asignar variables a los elementos de un tuple (igual pasa con las listas y los diccionarios)
t = (1,2,3)
x,y,z = t # Ojo: deben ser la misma cantidad de variables que de elementos del tuple
print(x,y,z)

t = ('a','b','c','a')
print(len(t)) # Rta: 4. Muestra cuántos elementos tiene el tuple
print(t.count('a')) # Rta: 2. Muestra cuántas veces se encuentra un elemento en el tuple
print(t.index('b')) # Rta: 1. Muestra en qué índice está un elemento del tuple






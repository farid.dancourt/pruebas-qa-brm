# LOS STRING SON INMUTABLES:
# Cambiar una letra del string:
nombre = "Carina"
print(nombre)
nombre = "Karina" # Lo puedo hacer porque reescribí la variable, no el string original
print(nombre)
#nombre[0] = "K"
#print(nombre) Rta: Error. No lo puedo hacer así porque el string es inmutable

# LOS STRING SON CONCATENABLES:
n1 = "Kari"
n2 = "na"
print(n1 + n2) # Los string se pueden concatenar

# LOS STRING SON MULTIPLICABLES:
n3 = "Pe"
print(n3*10) # Multiplica por 10 el string "Pe"

# LOS STRING SON MULTILINEALES:
poema = """Mil pequeños peces blancos
como si hirviera
el color del agua"""
print(poema) #Se usa la triple comilla doble para que valide los saltos de línea

# SE PUEDE VERIFICAR EL CONTENIDO DE LOS STRING:
print("agua" in poema) # Devuelve True porque "agua" sí está en poema
print("amor" in poema) # Devuelve False porque "agua" no está en poema
print("amor" not in poema) # Devuelve True porque "amor" no está en poema

# SE PUEDE DETERMINAR LA LONGITUD DE LOS STRING:
texto = "Parangaricutirimucuaro"
print(len(texto)) # Devuelve la longitud del contenido de texto (22)



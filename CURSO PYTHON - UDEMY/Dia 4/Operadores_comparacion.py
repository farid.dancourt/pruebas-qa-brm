mi_bool = 10 == 10 # Le asigna a la variable un valor booleano de acuerdo a la comparación
print(mi_bool) # Rta: True

mi_bool = '10' == 10
print(mi_bool) # Rta: False. Son valores diferentes ya que uno es string y el otro es integer

mi_bool = 10.0 == 10
print(mi_bool) # Rta: True. Aunque uno es float y el otro integer, su valor es el mismo

mi_bool = 100 != 100 # Verifica si son diferentes
print(mi_bool) # Rta: False: 100 no es diferente a 100




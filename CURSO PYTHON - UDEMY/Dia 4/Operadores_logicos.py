mi_bool = 4 < 5 and 5 > 6
print(mi_bool) # Rta: False porque la segunda condición no es verdadera

mi_bool = 4 < 5 and 5 == 2+3
print(mi_bool) # Rta: True

# se pueden usar paréntesis para ver mejor:
mi_bool = (4 < 5) and (5 == 2+3)
print(mi_bool) # Rta: True

texto = 'Esta frase es breve'
mi_bool = ('frase' in texto) and ('breve' in texto)
print(mi_bool) # Rta: True

texto = 'Esta frase es breve'
mi_bool = ('frase' in texto) or ('python' in texto)
print(mi_bool) # Rta: True. Una de las dos es verdadera.

mi_bool = not ('a' == 'a')
print(mi_bool) # False. La condición es verdadera pero el not la cambia

frase = "Cuando algo es lo suficientemente importante, lo haces incluso si las probabilidades de que salga bien no te acompañan"
palabra1 = 'éxito'
palabra2 = 'tecnología'
mi_bool = not ((palabra1 in frase) and (palabra2 in frase))
print(mi_bool) # Rta: True. Las palabras no están en la frase, pero el not hace que
               # la respuesta cambie de False a True
mascota = 'perro'

if mascota == 'gato': # Ojo: siempre va dos puntos (:) al final de la línea
    print('Tienes un gato')
elif mascota == 'loro': # Ojo: siempre va dos puntos (:) al final de la línea
    print('Tienes un loro')
elif mascota == 'perro':
    print('Tienes un perro')
else:
    print('No sé qué animal tienes')

# Los if se pueden anidar:

edad = 15
calificacion = 4

if edad < 18:
    print('Eres menor de edad')
    if calificacion >= 7:
        print('Aprobaste')
    else:
        print('No aprobaste')
else:
    print('Eres adulto')

# Otro ejemplo:
num1 = input("Ingresa un número:")
num2 = input("Ingresa otro número:")
if num1>num2:
    print(f"{num1} es mayor que {num2}")
elif num1<num2:
    print(f"{num1} es menor que {num2}")
else:
    print(f"{num1} y {num2} son iguales")

# Otro ejemplo: Para acceder a un trabajo, se necesita saber inglés y Python:

habla_ingles = True
sabe_python = False

if habla_ingles == True:
    if sabe_python == True:
        print("Cumples con los requisitos para postularte")
    else:
        print("Para postularte, necesitas saber programar en Python")
elif sabe_python == True:
    print("Para postularte, necesitas tener conocimientos de inglés")
else:
    print("Para postularte, necesitas saber programar en Python y tener conocimientos de inglés")


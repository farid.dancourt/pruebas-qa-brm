# Ejemplo 1: hacer una lista de cuatro letras e imprimir cada letra indicando su posición dentro de la lista:
print('EJEMPLO 1')
lista = ['a','b','c','d']
for letra in lista: # La variable 'letra' puede tener cualquier nombre ya que es una variable interna cuyo valor va
                    # cambiando a medida que el loop for aumenta, es decir, 'letra' toma el valor de 'a', luego de 'b'
                    # y así sucesivamente
    numeracion = lista.index(letra) + 1 # se suma el 1 ya que el primer índice es cero y necesitamos decir que las
                                        # letras empiezan desde 1
    print(f'La letra {numeracion} es: {letra}')
print('\n')

# Ejemplo 2: hacer una lista con nombres de personas e imprimir solamente los que empiecen con la letra 'F':
print('EJEMPLO 2')
lista = ['Julián', 'Farid', 'Diana', 'Andrés', 'Federico']
print('Nombres que empiezan con F:')
for nombre in lista:
    if nombre.startswith('F'):
        print(nombre)
print('\n')

# Ejemplo 3: hacer una lista con los números del 1 al 5 e imprimir la suma de todos esos números:
print('EJEMPLO 3')
lista = [1,2,3,4,5]
mi_numero = 0
for numero in lista:
    mi_numero = mi_numero + numero
print(f'La suma de los números de la lista es: {mi_numero}')
print('\n')

# No es necesario que se tengan los valores dentro de una lista:
palabra = 'python'
for letra in palabra:
    print(letra) # Rta: escribe cada letra dela palabra
print('\n')

# Tampoco es obligatorio que el dato esté en una variable:
for letra in 'pyhton':
    print(letra) # Rta: la misma anterior
print('\n')

# Se puede usar el loop directamente con una lista o una tupla:
for objeto in (2,7,3):
    print(objeto)
for objeto in [5,97,21]:
    print(objeto)
print('\n')

# Se pueden usar listas anidadas:
for objeto in [[1,2], [3,4], [5,6]]:
    print(objeto) # muestra cada sublista
print('\n')

# Mostrar todos los objetos de listas anidadas de forma separada:
for a,b in [[1,2], [3,4], [5,6]]:
    print(a) # Muestra el primer elemento de cada sublista (1, 3, 5)
    print(b) # Muestra el segundo elemento de cada sublista (2, 4, 6)
print('\n')

# Con diccionarios:
dic = {'clave1':'a', 'clave2':'b', 'clave3':'c'}
for objeto in dic:
    print(objeto) # Rta: muestra solamente las claves
for objeto in dic.items():
    print(objeto) # Rta: muestra parejas de claves y valores
for objeto in dic.values():
    print(objeto) # Rta: muestra solamente valores
for a,b in dic.items():
    print(a,b) # otra forma de ver clabes y valores juntos

